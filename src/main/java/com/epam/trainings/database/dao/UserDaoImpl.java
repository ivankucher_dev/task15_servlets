package com.epam.trainings.database.dao;

import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;
import com.epam.trainings.database.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class UserDaoImpl implements UserDao {

  @Override
  public Optional<User> getUserById(int id) {
    return Optional.of(
        HibernateSessionFactoryUtil.getSessionFactory().openSession().get(User.class, id));
  }

  @Override
  public Optional<Integer> getUserIdByName(String name) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    Query query = session.createQuery("from User where name = :name");
    query.setParameter("name", name);
    if (query.list().size() == 0) {
      return Optional.ofNullable(null);
    }
    User user = (User) query.list().get(0);
    Optional optUser = Optional.ofNullable(user);
    if (optUser.isPresent()) {
      return Optional.ofNullable(user.getId());
    }
    return Optional.ofNullable(user.getId());
  }

  @Override
  public boolean login(String name, int age) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    Query query = session.createQuery("from User where name = :name");
    query.setParameter("name", name);
    if (query.list().size() == 0) {
      return false;
    }
    Optional<User> user = Optional.ofNullable((User) query.list().get(0));
    if (user.isPresent()) {
      if (age == (user.get().getAge())) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  @Override
  public boolean saveUser(User user) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    Transaction tx1 = session.beginTransaction();
    session.save(user);
    tx1.commit();
    session.close();
    return true;
  }

  @Override
  public void updateUser(User user) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    session.beginTransaction();
    session.update(user);
    session.getTransaction().commit();
    session.close();
  }

  @Override
  public void deleteUserAuto(Auto auto) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    session.beginTransaction();
    session.delete(auto);
    session.getTransaction().commit();
    session.close();
  }


  @Override
  public void saveUserAuto(Auto auto) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    session.beginTransaction();
    session.save(auto);
    session.getTransaction().commit();
    session.close();
  }

  @Override
  public Optional<Auto> getAutoById(int id) {
    return Optional.ofNullable(
            HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Auto.class, id));
  }

  @Override
  public Optional<Set<Auto>> getAllUserAutos(int id) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    Optional<User> user = Optional.of(session.get(User.class, id));
    session.beginTransaction();
    session.close();
    if (user.isPresent()) {
      Set<Auto> autos = user.get().getAutos();
      return Optional.ofNullable(autos);
    } else {
      return Optional.ofNullable(new HashSet<Auto>());
    }
  }
}
