package com.epam.trainings.database.service;

import com.epam.trainings.customexceptions.NoEntityException;
import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {
  User getUserById(int id) throws NoEntityException;

  boolean login(String name, int age);

  Optional<Auto> getAutoById(int id);

  Optional<Integer> getUserIdByName(String name);

  boolean saveUser(User user);

  void updateUser(User user);

  void saveUserAuto(Auto auto);

  void deleteUserAuto(Auto auto);

  Set<Auto> getAllUserAutos(int id);
}
