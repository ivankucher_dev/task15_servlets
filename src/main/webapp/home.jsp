<%@ page import="com.epam.trainings.database.model.Auto" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Set" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <script type="text/javascript">
        function sendDelete(url) {
            var request = new XMLHttpRequest();
            request.open("DELETE", url, false);
            request.send(null);
        }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>:: Home ::</title>
</head>
<body>
<table width="100%">
    <tr bgcolor=F9F9F3>
        <td align="left"><h1>Login Success...</h1></td>
        <td align="right">
            <h2>
                Hi, <%= request.getAttribute("name") %>
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="2"><h1 align="center"></br></br>Your name
            :  <%= request.getAttribute("name") %>
        </h1></td>
        <td colspan="2"><h1 align="center"></br></br>Your age :  <%= request.getAttribute("age") %>
        </h1></td>
        <td colspan="2"><h1 align="center"></br></br>Your email
            :  <%= request.getAttribute("email") %>
        </h1></td>
        <%
            Set<Auto> set = (Set<Auto>) request.getAttribute("autos");
            int i = -1;
            if (set != null) {
                for (Iterator<Auto> it = set.iterator(); it.hasNext(); ) {
                    i++;
                    Auto auto = it.next();
        %>
        <h5>
            Cars :
            <%=i + 1%>
            ID : <%=auto.getId()%>
            Color : <%=auto.getColor()%>
            Model : <%=auto.getModel()%>
            <a href="users/autos/<%=auto.getId()%>/delete-car"
               onclick="sendDelete(this.href)">delete</a>
        </h5>
        <%
                }
            }
        %>
        <form action="users/autos/<%=request.getAttribute("user_id")%>/add-car" method="GET">
            <input type="submit" value="add car">
        </form>

        <form action="login" method="GET">
            <input type="submit" value="logout">
        </form>
    </tr>
</table>
</body>
</html>