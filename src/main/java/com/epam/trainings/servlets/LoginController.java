package com.epam.trainings.servlets;

import com.epam.trainings.customexceptions.NoEntityException;
import com.epam.trainings.database.model.User;
import com.epam.trainings.database.service.UserService;
import com.epam.trainings.database.service.UserServiceImpl;
import javassist.NotFoundException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class LoginController extends HttpServlet {

  private UserService loginService;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    response.sendRedirect("login.jsp");
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("name");
    int age = Integer.parseInt(request.getParameter("age"));
    System.out.println(username + " :: " + age);
    String page = "login.jsp";
    if (username.trim().length() >= 0 && username != null && age >= 0) {
      loginService = new UserServiceImpl();
      boolean flag = loginService.login(username, age);
      Optional<Integer> id = loginService.getUserIdByName(request.getParameter("name"));
      try {
        id.orElseThrow(() -> new NotFoundException("id not found!"));
      } catch (NotFoundException e) {
        e.printStackTrace();
      }
      if (id.isPresent()) {
        request.setAttribute("user_id", id.get());
        try {
          User user = getUser(id);
          request.setAttribute("autos", user.getAutos());
          request.setAttribute("email", user.getEmail());
        } catch (NoEntityException e) {
          e.printStackTrace();
        }
      }
      if (flag) {
        request.setAttribute("id", id);
        request.setAttribute("name", username);
        request.setAttribute("age", age);
        request.setAttribute("msg", "Login Success...");
        page = "home.jsp";
      } else {
        request.setAttribute("msg", "Wrong Username or Age, Try again!!!");
      }
    } else {
      request.setAttribute("msg", "Please enter username and age...");
    }
    request.getRequestDispatcher("home.jsp").forward(request, response);
  }

  private User getUser(Optional<Integer> id) throws NoEntityException {
    return loginService.getUserById(id.get());
  }
}
