package com.epam.trainings.database.model;

import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Auto {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String model;
  private String color;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User owner;

  public Auto(String model, String color,User owner) {
    this.model = model;
    this.color = color;
    this.owner = owner;
  }

  public Auto(){}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }
}
