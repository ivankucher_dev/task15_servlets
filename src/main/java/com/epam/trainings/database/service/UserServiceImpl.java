package com.epam.trainings.database.service;

import com.epam.trainings.customexceptions.NoEntityException;
import com.epam.trainings.database.dao.UserDao;
import com.epam.trainings.database.dao.UserDaoImpl;
import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class UserServiceImpl implements UserService {

  private UserDao userDao;

  public UserServiceImpl() {
    this.userDao = new UserDaoImpl();
  }

  @Override
  public User getUserById(int id) throws NoEntityException {
    return userDao.getUserById(id).orElseThrow(() -> new NoEntityException(id));
  }

  @Override
  public boolean login(String name, int age) {
    return userDao.login(name, age);
  }

  @Override
  public Optional<Auto> getAutoById(int id) {
    return userDao.getAutoById(id);
  }

  @Override
  public Optional<Integer> getUserIdByName(String name) {
    return userDao.getUserIdByName(name);
  }

  @Override
  public boolean saveUser(User user) {
    return userDao.saveUser(user);
  }

  @Override
  public void updateUser(User user) {
    userDao.updateUser(user);
  }

  @Override
  public void saveUserAuto(Auto auto) {
    userDao.saveUserAuto(auto);
  }

  @Override
  public void deleteUserAuto(Auto auto) {
    userDao.deleteUserAuto(auto);
  }

  @Override
  public Set<Auto> getAllUserAutos(int id) {
    return userDao.getAllUserAutos(id).orElse(new HashSet<Auto>());
  }
}
