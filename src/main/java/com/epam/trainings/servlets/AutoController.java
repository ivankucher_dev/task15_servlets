package com.epam.trainings.servlets;

import com.epam.trainings.customexceptions.NoEntityException;
import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;
import com.epam.trainings.database.service.UserService;
import com.epam.trainings.database.service.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class AutoController extends HttpServlet {

  private static Logger log = LogManager.getLogger(AutoController.class.getName());
  private int id;
  private UserService service;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    id =
        Integer.parseInt(
            request
                .getPathInfo()
                .replace("users", "")
                .replace("add-car", "")
                .replace("autos", "")
                .replaceAll("/", "")
                .trim());
    request.setAttribute("user_id", id);
    request.getRequestDispatcher("/addcar.jsp").forward(request, response);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    service = new UserServiceImpl();
    String model = request.getParameter("model");
    String color = request.getParameter("color");
    int id = Integer.parseInt(request.getParameter("owner"));
    String page = "home.jsp";
    try {
      User user = service.getUserById(id);
      request.setAttribute("name", user.getName());
      request.setAttribute("email", user.getEmail());
      request.setAttribute("age", user.getAge());
      request.setAttribute("autos", user.getAutos());
    } catch (NoEntityException e) {
      e.printStackTrace();
    }
    if (model.trim().length() >= 0 && color.length() >= 0 && id >= 0) {
      try {
        User user = service.getUserById(id);
        service.saveUserAuto(new Auto(model, color, user));
      } catch (NoEntityException e) {
        e.printStackTrace();
      }
      request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    int autoId =
        Integer.parseInt(req.getPathInfo().trim().replaceAll("[a-zA-Z]", "").replaceAll("/", ""));

    Optional<Auto> auto = service.getAutoById(autoId);

    auto.ifPresent(this::deleteAuto);
    req.getRequestDispatcher("/home.jsp").forward(req, resp);
  }

  private void deleteAuto(Auto auto) {
    service.deleteUserAuto(auto);
  }
}
