package com.epam.trainings.database.utils;

import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {

  public static SessionFactory sessionFactory;
  private static Logger log = LogManager.getLogger(HibernateSessionFactoryUtil.class.getName());

  public static SessionFactory getSessionFactory() {
    if (sessionFactory == null) {
      try {
        Configuration configuration = new org.hibernate.cfg.Configuration().configure();
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Auto.class);
        StandardServiceRegistryBuilder bulilder =
            new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(bulilder.build());
      } catch (Exception e) {
        log.error(e);
      }
    }
    return sessionFactory;
  }
}
