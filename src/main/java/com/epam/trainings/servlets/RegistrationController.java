package com.epam.trainings.servlets;

import com.epam.trainings.database.model.User;
import com.epam.trainings.database.service.UserService;
import com.epam.trainings.database.service.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.trainings.database.utils.PropertiesReader.getProperty;

public class RegistrationController extends HttpServlet {
  private static Logger log = LogManager.getLogger(RegistrationController.class.getName());

  public RegistrationController() {
    super();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    response.sendRedirect("userRegistration.jsp");
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    String msg = "Something went wrong";
    String page = "userRegistration.jsp";
    User user = new User();
    user.setName(request.getParameter("name"));
    user.setAge(Integer.parseInt(request.getParameter("age")));
    user.setEmail(request.getParameter("firstName"));
    UserService registerService = new UserServiceImpl();
    log.info(user);
    boolean regStatus = registerService.saveUser(user);
    if (regStatus) {
      msg = getProperty("login_status_ok");
    } else {
      msg = getProperty("login_status_error");
    }
    page = "login.jsp";
    request.setAttribute("msg2", msg);
    request.getRequestDispatcher(page).include(request, response);
  }
}
