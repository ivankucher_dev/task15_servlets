package com.epam.trainings.database.dao;

import com.epam.trainings.database.model.Auto;
import com.epam.trainings.database.model.User;

import java.util.Optional;
import java.util.Set;

public interface UserDao {
  Optional<User> getUserById(int id);

  Optional<Integer> getUserIdByName(String name);

  void saveUserAuto(Auto auto);

  Optional<Auto> getAutoById(int id);

  boolean login(String name, int age);

  boolean saveUser(User user);

  void updateUser(User user);

  void deleteUserAuto(Auto auto);

  Optional<Set<Auto>> getAllUserAutos(int id);
}
