package com.epam.trainings.database.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String name;
  private int age;
  private String email;

  @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
  private Set<Auto> autos;

  public User(String name, int age, String email) {
    this.name = name;
    this.age = age;
    this.email = email;
  }

  public User() {}

  public Set<Auto> getAutos() {
    return autos;
  }

  public void setAutos(Set<Auto> autos) {
    this.autos = autos;
  }

  public void addAuto(Auto auto){
    autos.add(auto);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
