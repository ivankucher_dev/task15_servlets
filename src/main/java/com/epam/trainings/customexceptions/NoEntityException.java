package com.epam.trainings.customexceptions;

public class NoEntityException extends Exception {
  public NoEntityException(int userId) {
    super("No user with id - " + userId + " found. Check your data before access");
  }
}
