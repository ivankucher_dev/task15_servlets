package com.epam.trainings.main;

import com.epam.trainings.database.service.UserService;
import com.epam.trainings.database.service.UserServiceImpl;
import com.epam.trainings.database.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;

import java.util.Optional;

public class Main {
  public static void main(String[] args) {
    Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
    UserService loginService = new UserServiceImpl();
    boolean flag = loginService.login("ivan", 19);
    Optional<Integer> id = loginService.getUserIdByName("ivan");
  }
}
