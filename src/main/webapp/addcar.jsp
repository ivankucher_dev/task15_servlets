<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add car</title>
</head>
<body>
</br></br></br></br>
<div align="center">
    <form action="users/autos/add-car" method="post">
        <table align="center" border="1">
            <tr>
                <td colspan="2" align="center"><b>Car registration</b></td>
            </tr>
            <tr>
                <td>Car model* :</td>
                <td><input type="text" name="model" required></td>
            </tr>
            <tr>
                <td>Color* :</td>
                <td><input type="text" name="color" required></td>
            </tr>
            <tr>
                <td>Owner* :</td>
                <td><input type="text" name="owner" value="<%=request.getAttribute("user_id")%>" readonly></td>
            </tr>
            <tr>
                <td align="center"><input type="submit" value="Register car"></td>
                <td align="center"><input type="reset" name="Reset Form"></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>